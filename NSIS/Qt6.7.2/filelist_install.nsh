;Skapad av nsisFileList 1.2.14
;Mon Jul 8 01:08:24 2024
;Copyright (C) 2022 - 2024 Ingemar Ceicer
;Licens GNU General Public License v3.0
;https://gitlab.com/posktomten/nsisfilelist


;Add/Install Files
SetOutPath "$INSTDIR"
file "D3Dcompiler_47.dll"
file "dxcompiler.dll"
file "libc++.dll"
file "libcrypto-3-x64.dll"
file "libssl-3-x64.dll"
file "libunwind.dll"
file "opengl32sw.dll"
file "Qt6Core.dll"
file "Qt6Gui.dll"
file "Qt6Network.dll"
file "Qt6Pdf.dll"
file "Qt6Svg.dll"
file "Qt6Widgets.dll"
file "qt-style.exe"
file "icon.ico"
file "unicon.ico"

SetOutPath "$INSTDIR\platforms"
file "platforms\qwindows.dll"

SetOutPath "$INSTDIR\styles"
file "styles\qmodernwindowsstyle.dll"

SetOutPath "$INSTDIR\tls"
file "tls\qcertonlybackend.dll"
file "tls\qopensslbackend.dll"
file "tls\qschannelbackend.dll"

SetOutPath "$INSTDIR\generic"
file "generic\qtuiotouchplugin.dll"

SetOutPath "$INSTDIR\iconengines"
file "iconengines\qsvgicon.dll"

SetOutPath "$INSTDIR\imageformats"
file "imageformats\qgif.dll"
file "imageformats\qicns.dll"
file "imageformats\qico.dll"
file "imageformats\qjpeg.dll"
file "imageformats\qpdf.dll"
file "imageformats\qsvg.dll"
file "imageformats\qtga.dll"
file "imageformats\qtiff.dll"
file "imageformats\qwbmp.dll"
file "imageformats\qwebp.dll"

SetOutPath "$INSTDIR\License"

SetOutPath "$INSTDIR\networkinformation"
file "networkinformation\qnetworklistmanager.dll"
