;Skapad av nsisFileList 1.2.14
;Mon Jul 8 02:11:03 2024
;Copyright (C) 2022 - 2024 Ingemar Ceicer
;Licens GNU General Public License v3.0
;https://gitlab.com/posktomten/nsisfilelist


;Add/Install Files
SetOutPath "$INSTDIR"
file "libssl-3-x64.dll"
file "libstdc++-6.dll"
file "libwinpthread-1.dll"
file "msvcr100.dll"
file "Qt5Core.dll"
file "Qt5Gui.dll"
file "Qt5Network.dll"
file "Qt5Svg.dll"
file "Qt5Widgets.dll"
file "qt-style.exe"
file "unicon.ico"
file "icon.ico"
file "libcrypto-3-x64.dll"
file "libgcc_s_seh-1.dll"

SetOutPath "$INSTDIR\bearer"
file "bearer\qgenericbearer.dll"

SetOutPath "$INSTDIR\iconengines"
file "iconengines\qsvgicon.dll"

SetOutPath "$INSTDIR\imageformats"
file "imageformats\qgif.dll"
file "imageformats\qicns.dll"
file "imageformats\qico.dll"
file "imageformats\qjpeg.dll"
file "imageformats\qsvg.dll"
file "imageformats\qtga.dll"
file "imageformats\qtiff.dll"
file "imageformats\qwbmp.dll"
file "imageformats\qwebp.dll"

SetOutPath "$INSTDIR\License"

SetOutPath "$INSTDIR\platforms"
file "platforms\qwindows.dll"

SetOutPath "$INSTDIR\styles"
file "styles\qwindowsvistastyle.dll"
