
#ifndef VERSION_H
#define VERSION_H

#define VER_FILEVERSION             1,0,5,0
#define VER_FILEVERSION_STR         "1.0.5.0\0"

#define VER_PRODUCTVERSION          1,0,5,0
#define VER_PRODUCTVERSION_STR      "1.0.5"
#define VER_COMPANYNAME_STR         "Ingemar Ceicer"
#define VER_FILEDESCRIPTION_STR     "Testing different Qt styles."
#define VER_INTERNALNAME_STR        "Testing different Qt styles."
#define VER_LEGALCOPYRIGHT_STR      "Copyright (C) 2024 Ingemar Ceicer"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "qt-style.exe"
#define VER_PRODUCTNAME_STR         "Qt-style"

#define VER_COMPANYDOMAIN_STR       "gitlab.com/posktomten/qt-style"

#endif // VERSION_H
