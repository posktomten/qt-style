// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

// qt-style Copyright (C) 2024 Ingemar Ceicer
// https://gitlab.com/posktomten/qt-style/wikis/home
// programming@ceicer.com

// A program to test how a Qt GUI program looks depending on which "Style" you choose.
// Which styles are available to choose from depends on the operating system and Qt version.

// qt-style is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
// And NO LATER versions are allowed.
// GPL Version 3.0 only.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGlobal>
#ifdef Q_OS_LINUX
#include "updatedialog.h"
#endif
#ifdef Q_OS_WIN
#define DOWNLOAD_INSTALL
#endif
#define VERSION "1.0.5"

#if defined DOWNLOAD_INSTALL && defined Q_OS_WIN
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#define PATH "https://bin.ceicer.com/qt-style/bin/windows/obsolete/"
#if defined(Q_PROCESSOR_X86_64) // Windows 64 bit
#define FILENAME "qt-style_64-bit_setup.exe"
#elif defined (Q_PROCESSOR_X86_32) // Windows 32 bit
#define FILENAME "qt-style_32-bit_setup.exe"
#endif

#elif  QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
#define PATH "https://bin.ceicer.com/qt-style/bin/windows/"
#define FILENAME "qt-style_64-bit_setup.exe"
#endif // QT_VERSION
#endif // DOWNLOAD_INSTALL && defined Q_OS_WIN

#define DISPLAY_NAME "Qt-style"
#define EXECUTABLE_NAME "qt-style"
#define COPYRIGHT "Ingemar Ceicer"
#define COPYRIGHT_YEAR "2024"
#define EMAIL "programming@ceicer.com"
#define BUILD_DATE_TIME __DATE__ " " __TIME__
#define LICENSE "Gnu General Public License Version 3"
#define LICENSE_LINK "https://gitlab.com/posktomten/qt-style/-/blob/master/LICENSE"
#define CHANGELOG "https://gitlab.com/posktomten/qt-style/-/blob/master/CHANGELOG"
#define SOURCECODE "https://gitlab.com/posktomten/qt-style"
#define WEBSITE "https://gitlab.com/posktomten/qt-style/-/wikis/home"

// chortcut
#define COMMENTS "Program to test AppImage that updates itself."
#define CATEGORIES "Utility"

#if defined(Q_OS_WIN) // WINDOWS
#define DOWNLOAD_SERVER "https://bin.ceicer.com/qt-style/bin/windows/"
#define COMPILEDON "Windows 11 Pro 23H2<br>OS build: 22631.3737"
#define VERSION_PATH "https://bin.ceicer.com/qt-style/version_windows.txt"
#endif // WINDOWS



#if defined(Q_OS_LINUX) // LINUX
#define DOWNLOAD_SERVER "https://bin.ceicer.com/qt-style/bin/linux/"
#define VERSION_PATH "https://bin.ceicer.com/qt-style/version_linux.txt"

#if  (__GLIBC_MINOR__ == 39)
#define COMPILEDON "Ubuntu 24.04 LTS 64-bit, GLIBC 2.39"
#define ARG1 "qt-style-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/qt-style/GLIBC2.39/qt-style-x86_64.AppImage.zsync"
#endif // __GLIBC_MINOR__ == 39

#if  (__GLIBC_MINOR__ == 35)
#define COMPILEDON "Ubuntu 22.04.4 LTS 64-bit, GLIBC 2.35"
#define ARG1 "qt-style-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/qt-style/GLIBC2.35/qt-style-x86_64.AppImage.zsync"
#endif // __GLIBC_MINOR__ == 35

#if (__GLIBC_MINOR__ == 31)
#define COMPILEDON "Lubuntu 20.04.6 LTS 64-bit, GLIBC 2.31"
#define ARG1 "qt-style-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/qt-style/GLIBC2.31/qt-style-x86_64.AppImage.zsync"
#endif // __GLIBC_MINOR__ == 31

#if  (__GLIBC_MINOR__ == 27)
#if defined(Q_PROCESSOR_X86_64) // 64 bit
#define COMPILEDON "Lubuntu 18.04.6 LTS 64-bit, GLIBC 2.27"
#define ARG1 "qt-style-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/qt-style/GLIBC2.27/qt-style-x86_64.AppImage.zsync"
#elif defined(Q_PROCESSOR_X86_32) // 32 bit
#define COMPILEDON "Lubuntu 18.04.6 LTS 32-bit, GLIBC 2.27"
#define ARG1 "qt-style-i386.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/qt-style/GLIBC2.27/qt-style-i386.AppImage.zsync"
#endif // Q_PROCESSOR_

#endif // __GLIBC_MINOR__ == 27

#endif //  LINUX

QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWindow;
}

QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void setEndConfig();

private:
    Ui::MainWindow *ui;
    bool theStyle(QString style);
    void setStartConfig(QIcon *icon);
    bool savesettings;
#if defined Q_OS_WIN && defined DOWNLOAD_INSTALL
    void downloadInstall();
    void uninstall();
#endif
#ifdef Q_OS_LINUX
    UpdateDialog *ud;
#endif

    // void colorSchemeChanged(QEvent *event);
private slots:
    void about();
    void doupdate(bool update);
#ifdef Q_OS_LINUX
    void isUpdated(bool uppdated);


#endif

};

#endif // MAINWINDOW_H
