// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

// qt-style Copyright (C) 2024 Ingemar Ceicer
// https://gitlab.com/posktomten/qt-style/wikis/home
// programming@ceicer.com

// A program to test how a Qt GUI program looks depending on which "Style" you choose.
// Which styles are available to choose from depends on the operating system and Qt version.

// qt-style is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
// And NO LATER versions are allowed.
// GPL Version 3.0 only.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include <QMainWindow>
#include <QProcess>
#include <QStyleFactory>
#include <QSettings>
#include <QActionGroup>
#include <QFileInfo>
#include <QSysInfo>
#include <QMessageBox>
#include <QCoreApplication>
#include <QStyleHints>
#include <QStyle>
#include "ui_mainwindow.h"
#include "mainwindow.h"
#include "qstringliteral.h"
#include "checkupdate.h"
#ifdef Q_OS_LINUX
#include "update.h"
#include "updatedialog.h"
#endif
#if defined Q_OS_LINUX
#include "createshortcut_lin.h"
#elif defined Q_OS_WIN
#include "createshortcut_win.h"
#endif
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    savesettings = true;
    ui->setupUi(this);
    // qDebug() << QApplication::styleHints();
    // qDebug() << QGuiApplication::styleHints();
    // if(QGuiApplication::styleHints()->colorScheme() == Qt::ColorScheme::Light) {
    //     qDebug() << "LIGHT";
    // }  else {
    //     qDebug() << "DARK";
    // }
#ifdef Q_OS_LINUX
    ui->actionUpdate->setVisible(true);
    ui->actionUpdate->setText(tr("Update..."));
    ui->actionApplicationMenuShortcut->setVisible(true);
    ui->actionUninstall->setVisible(false);
#endif
#if defined Q_OS_WIN && defined DOWNLOAD_INSTALL
    ui->actionUpdate->setVisible(true);
    ui->actionUninstall->setVisible(true);
    ui->actionApplicationMenuShortcut->setVisible(false);
#elif defined Q_OS_WIN && !defined DOWNLOAD_INSTALL
    ui->actionUpdate->setVisible(false);
    ui->actionUninstall->setVisible(false);
#endif
    QIcon *icon = new QIcon(QStringLiteral(":images/icon.png"));
    setStartConfig(icon);
    QObject::connect(ui->actionAbout, &QAction::triggered, this, &MainWindow::about);
    setWindowIcon(*icon);
    this->setWindowTitle(DISPLAY_NAME " " VERSION);
    /*** STYLE ***/
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Style");
    QString currentstyle = settings.value(QStringLiteral("currentstyle"), QStringLiteral("Fusion")).toString();
    settings.endGroup();
    QStringList styles;
    styles << QStyleFactory::keys();
    ui->txtBrowser->setText(QStringLiteral("<h4>Available styles</h4>"));
    QActionGroup *actionGroupStil = new QActionGroup(this);
    styles.sort(Qt::CaseInsensitive);
    QString stile;
#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)

    for(const QString &style : std::as_const(styles)) {
#else

    for(const QString &style : qAsConst(styles)) {
#endif
        QAction *stil = new QAction(style);
        stil->setCheckable(true);

        if(currentstyle == stil->text()) {
            stil->setChecked(true);
            stile += QStringLiteral("<b>") + stil->text() +  QStringLiteral("(Current)</b><br>");
        } else {
            stile += stil->text() + QStringLiteral("<br>");
        }

        actionGroupStil->addAction(stil);
        ui->menuStyles->addAction(stil);
        // SELECT Style
        connect(stil, &QAction::triggered, this, [ stil, this]() {
            if(theStyle(stil->text())) {
                stil->setChecked(true);
                const QString EXECUTE =
                    QCoreApplication::applicationDirPath() + QStringLiteral("/") +
                    QFileInfo(QCoreApplication::applicationFilePath()).fileName();
                QProcess p;
                p.setProgram(EXECUTE);
                p.startDetached();
                close();
            } else {
                stil->setChecked(true);
            }
        });

        // END SELECT Style
    }

    ui->txtBrowser->append(stile);
    actionGroupStil->setExclusionPolicy(QActionGroup::ExclusionPolicy::ExclusiveOptional);
    ui->lblOsQt->setText(QStringLiteral("Operating system: ") + QSysInfo::kernelType() + ' ' + QSysInfo::prettyProductName() + ' ' + QSysInfo::currentCpuArchitecture() + QStringLiteral("<br>Qt version: ") + QT_VERSION_STR);
    /*** END STYLE ***/
    connect(ui->actionExit, &QAction::triggered, [this]() {
        close();
    });

    /*** Remove all files **/
    QObject::connect(ui->actionDeleteAllSettingd, &QAction::triggered, [this]() {
        QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
        QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
        QString *version = new QString(QStringLiteral(VERSION));
#ifdef Q_OS_LINUX
        QIcon *icon = new QIcon(QStringLiteral(":images/icon.png"));

        if(Createshortcut::deleteAllSettings(display_name, executable_name, version, icon)) {
            savesettings = false;
            close();
        }

#endif
#ifdef Q_OS_WIN
#ifdef DOWNLOAD_INSTALL

        if(Createshortcut::deleteAllSettings(display_name, executable_name, version, true)) {
            savesettings = false;
            close();
        }

#else

        if(Createshortcut::deleteAllSettings(display_name, executable_name, version, false)) {
            savesettings = false;
            close();
        }

#endif
#endif
    });

    connect(ui->actionApplicationMenuShortcut, &QAction::triggered, [this]() {
        QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);

        if(ui->actionApplicationMenuShortcut->isChecked()) {
            QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
#if defined(Q_OS_LINUX)
            QString *comments = new QString(QStringLiteral(COMMENTS));
            QString *categories = new QString(QStringLiteral(CATEGORIES));
            QString *icon  = new QString(QStringLiteral(EXECUTABLE_NAME".png"));
            Createshortcut::makeShortcutFile(display_name, executable_name, comments, categories, icon, true, false);
#endif
#if defined(Q_OS_WIN)
            Createshortcut::makeShortcutFile(display_name, executable_name, true, false);
#endif
#if defined(Q_OS_LINUX)
            Createshortcut::makeShortcutFile(display_name, executable_name, comments, categories, icon, true, false);
#endif
            settings.beginGroup("Settings");
            settings.setValue("applicationsmenushortcut", true);
            settings.endGroup();
        } else {
            Createshortcut::removeApplicationShortcut(executable_name);
            settings.beginGroup("Settings");
            settings.setValue("applicationsmenushortcut", false);
            settings.endGroup();
        }
    });

    connect(ui->actionDesktopShortcut, &QAction::triggered, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));

        if(ui->actionDesktopShortcut->isChecked()) {
            QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
#if defined(Q_OS_LINUX)
            QString *comments = new QString(QStringLiteral(COMMENTS));
            QString *categories = new QString(QStringLiteral(CATEGORIES));
            QString *icon  = new QString(QStringLiteral(EXECUTABLE_NAME".png"));
            Createshortcut::makeShortcutFile(display_name, executable_name, comments, categories, icon, false, true);
#endif
#if defined(Q_OS_WIN)
            Createshortcut::makeShortcutFile(display_name, executable_name, false, true);
#endif
            settings.beginGroup("Settings");
            settings.setValue("desktopshortcut", true);
            settings.endGroup();
        } else {
            Createshortcut::removeDesktopShortcut(executable_name);
            settings.beginGroup("Settings");
            settings.setValue("desktopshortcut", false);
            settings.endGroup();
        }
    });

    connect(ui->pbExit, &QPushButton::clicked, ui->actionExit, &QAction::triggered);
    /*****/
#if defined (Q_OS_LINUX) || (defined DOWNLOAD_INSTALL && defined Q_OS_WIN)
    connect(ui->actionUpdate, &QAction::triggered, [this]() {
#ifdef Q_OS_LINUX
        // QObject class
        QIcon *icon  = new QIcon(QStringLiteral(EXECUTABLE_NAME".png"));
        Update *up = new Update;
        connect(up, &Update::isUpdated, this, &MainWindow::isUpdated);
        // QDialog class
        ud = new UpdateDialog;
        ud->viewUpdate(icon);
        ud->show();
        up->doUpdate(ARG1, ARG2, DISPLAY_NAME, ud, icon);
#endif // Q_OS_LINUX
#if defined DOWNLOAD_INSTALL && defined Q_OS_WIN
        downloadInstall();
#endif //
    });

#endif // (Q_OS_LINUX) || (defined DOWNLOAD_INSTALL && defined Q_OS_WIN)
    /** UNINSTALL **/
#if defined DOWNLOAD_INSTALL && defined Q_OS_WIN
    connect(ui->actionUninstall, &QAction::triggered, [this]() {
        uninstall();
    });

#endif //DOWNLOAD_INSTALL && defined Q_OS_WIN
    /** END UNINSTALL **/
    /*****/
    connect(ui->actionCheckForUpdates, &QAction::triggered, [icon, this]() {
#if defined (Q_OS_LINUX)
        QString *updateinstructions =
            new QString(QStringLiteral("Please download the latest AppImage<br>Click on \"Tools\", \"Update...\"."));
#endif
#if !defined DOWNLOAD_INSTALL && defined Q_OS_WIN
        QString *updateinstructions =
            new QString(QStringLiteral("Please download the latest <a style=\"text-decoration: none\" href=\"" DOWNLOAD_SERVER "\">Portable</a>"));
#endif
#if defined DOWNLOAD_INSTALL && defined Q_OS_WIN
        QString *updateinstructions =
            new QString(tr("Please click on \"Tools\" and \"Update or Uninstall...\""));
#endif
#if defined (Q_OS_LINUX) || (defined DOWNLOAD_INSTALL && defined Q_OS_WIN)
#endif
        auto *cfu = new CheckUpdate;
        QObject::connect(cfu, &CheckUpdate::foundUpdate, this, &MainWindow::doupdate);
        cfu->check(DISPLAY_NAME, VERSION, VERSION_PATH, *updateinstructions, icon);
    });
}

bool MainWindow::theStyle(QString style)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Style");
    QString currentstyle = settings.value(QStringLiteral("currentstyle"), QStringLiteral("Fusion")).toString();
    settings.endGroup();

    if(currentstyle == style) {
        return false;
    }

    if(!ui->chbRestart->isChecked()) {
        QMessageBox *msgBox = new QMessageBox(this);
        msgBox->setText(QStringLiteral("<b>Warning!</b>"));
        msgBox->setInformativeText(QStringLiteral("The program must be restarted for the new settings to take effect.<br>Restart now?"));
        msgBox->setIcon(QMessageBox::Warning);
        msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
        QIcon iconReject(QPixmap(QStringLiteral(":/images/no.png")));
        QIcon iconAccept(QPixmap(QStringLiteral(":/images/yes.png")));
        QPushButton  *rejectButton = new QPushButton(iconReject, QStringLiteral("No"), msgBox);
        QPushButton  *laterButton = new QPushButton(QStringLiteral("Later"), msgBox);
        QPushButton  *acceptButton = new QPushButton(iconAccept, QStringLiteral("Yes"), msgBox);
        msgBox->addButton(acceptButton, QMessageBox::YesRole);
        msgBox->addButton(laterButton, QMessageBox::RejectRole);
        msgBox->addButton(rejectButton, QMessageBox::RejectRole);
        msgBox->setDefaultButton(rejectButton);
        msgBox->exec();

        if(msgBox->clickedButton() == acceptButton) {
            settings.beginGroup(QStringLiteral("Style"));
            settings.setValue(QStringLiteral("currentstyle"), style);
            settings.endGroup();
            settings.sync();
            delete msgBox;
            return true;
        } else  if(msgBox->clickedButton() == laterButton) {
            settings.beginGroup(QStringLiteral("Style"));
            settings.setValue(QStringLiteral("currentstyle"), style);
            settings.endGroup();
            delete msgBox;
            return false;
        } else  {
            delete msgBox;
            return false;
        }
    } else {
        settings.beginGroup(QStringLiteral("Style"));
        settings.setValue(QStringLiteral("currentstyle"), style);
        settings.endGroup();
        settings.sync();
        return true;
    }
}

void MainWindow::setStartConfig(QIcon *icon)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup(QStringLiteral("Settings"));
    this->restoreState(settings.value(QStringLiteral("state")).toByteArray());
    this->restoreGeometry(settings.value(QStringLiteral("geometry")).toByteArray());

    if(settings.value(QStringLiteral("radio")).toBool()) {
        ui->radioButton->setChecked(true);
    } else {
        ui->radioButton_2->setChecked(true);
    }

    ui->checkBox->setChecked(settings.value(QStringLiteral("checkbox")).toBool());
    ui->checkBox_2->setChecked(settings.value(QStringLiteral("checkbox_2")).toBool());
    ui->chbRestart->setChecked(settings.value(QStringLiteral("reboot_immediately"), true).toBool());
    bool checkonstart = settings.value(QStringLiteral("checkonstart"), true).toBool();
    ui->actionCheckForUpdatesWhenTheProgramStarts->setChecked(checkonstart);
    ui->actionApplicationMenuShortcut->setChecked(settings.value(QStringLiteral("applicationsmenushortcut"), false).toBool());
    ui->actionDesktopShortcut->setChecked(settings.value(QStringLiteral("desktopshortcut"), false).toBool());
    settings.endGroup();
    ui->comboBox->addItem(QStringLiteral("Windows 7"));
    ui->comboBox->addItem(QStringLiteral("Windows 10"));
    ui->comboBox->addItem(QStringLiteral("Lubuntu 18.04"));
    ui->comboBox->addItem(QStringLiteral("Ubuntu 22.04"));

    if(checkonstart) {
#if defined (Q_OS_LINUX)
        QString *updateinstructions =
            new QString(QStringLiteral("Please download the latest AppImage<br>Click on \"Tools\", \"Update...\"."));
#endif
#if !defined DOWNLOAD_INSTALL && defined Q_OS_WIN
        QString *updateinstructions =
            new QString(QStringLiteral("Please download the latest <a style=\"text-decoration: none\" href=\"" DOWNLOAD_SERVER "\">Portable</a>"));
#endif
#if defined DOWNLOAD_INSTALL && defined Q_OS_WIN
        QString *updateinstructions =
            new QString(tr("Please click on \"Tools\" and \"Update or Uninstall...\""));
#endif
#if defined (Q_OS_LINUX) || (defined DOWNLOAD_INSTALL && defined Q_OS_WIN)
#endif
        auto *cfu = new CheckUpdate;
        QObject::connect(cfu, &CheckUpdate::foundUpdate, this, &MainWindow::doupdate);
        cfu->checkOnStart(DISPLAY_NAME, VERSION, VERSION_PATH, *updateinstructions, icon);
    }
}



void MainWindow::setEndConfig()
{
    if(savesettings) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup(QStringLiteral("Settings"));
        settings.setValue(QStringLiteral("geometry"), saveGeometry());
        settings.setValue(QStringLiteral("state"), saveState());
        settings.setValue(QStringLiteral("radio"), ui->radioButton->isChecked());
        settings.setValue(QStringLiteral("checkbox"), ui->checkBox->isChecked());
        settings.setValue(QStringLiteral("checkbox_2"), ui->checkBox_2->isChecked());
        settings.setValue(QStringLiteral("reboot_immediately"), ui->chbRestart->isChecked());
        settings.setValue(QStringLiteral("checkonstart"), ui->actionCheckForUpdatesWhenTheProgramStarts->isChecked());
        settings.endGroup();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

// public slots:
void MainWindow::doupdate(bool update)
{
    if(update) {
        ui->actionUpdate->setEnabled(true);
    } else {
        ui->actionUpdate->setEnabled(false);
    }
}

#ifdef Q_OS_LINUX
void MainWindow::isUpdated(bool uppdated)
{
    if(uppdated) {
        close();
    }
}

#endif
// void MainWindow::colorSchemeChanged(QEvent *event)
// {
//     qDebug() << "EVENT " << event;
// }
