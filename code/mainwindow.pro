QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17
TARGET = qt-style
DESTDIR=../build-executable5
# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    about.cpp \
    download_install.cpp


HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui
    
RESOURCES += \
        resource.qrc

RC_FILE = myapp.rc

RC_ICONS = images/icon.ico

INCLUDEPATH = ../include


contains(QT_ARCH, i386) {

    contains(QMAKE_CC, gcc){
        # gcc
        # CONFIG (release, debug|release): LIBS += -L../lib5_32/ -labout # Release
        # else: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -laboutd # Debug
          LIBS += -L../lib5_32/ -labout
          LIBS += -L../lib5_32/ -lcheckupdate
          LIBS += -L../lib5_32/ -lcreateshortcut
          unix:LIBS += -L../lib5_32/ -lupdateappimage
          win32:LIBS += -L../lib5_32/ -ldownload_install
    }
      # Visual C++
    contains(QMAKE_CC, cl){
        # CONFIG (release, debug|release): LIBS += -L../lib5_32_msvc/ -labout # Release
        # else: CONFIG (debug, debug|release): LIBS += -L../../lib5_32_msvc/ -laboutd # Debug
          LIBS += -L../lib5_32_msvc/ -labout
          LIBS += -L../lib5_32_msvc/ -lcheckupdate
          LIBS += -L../lib5_32_msvc/ -lcreateshortcut
    }
}
contains(QT_ARCH, x86_64) {

equals(QT_PATCH_VERSION, 2) {


contains(QMAKE_CC, gcc){
    # gcc
    # CONFIG (release, debug|release): LIBS += -L../lib5/ -labout # Release
    # else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -laboutd # Debug
      LIBS += -L../lib_Qt5.15.2/ -labout
      LIBS += -L../lib_Qt5.15.2/ -lcheckupdate
      LIBS += -L../lib_Qt5.15.2/ -lcreateshortcut

}
  # Visual C++
contains(QMAKE_CC, cl){
    # CONFIG (release, debug|release): LIBS += -L../lib5_msvc/ -labout # Release
    # else: CONFIG (debug, debug|release): LIBS += -L../../lib5_msvc/ -laboutd # Debug
      LIBS += -L../lib_Qt5.15.2_msvc/ -labout
      LIBS += -L../lib_Qt5.15.2_msvc/ -lcheckupdate
      LIBS += -L../lib_Qt5.15.2_msvc/ -lcreateshortcut
}
}



}
greaterThan(QT_PATCH_VERSION, 2) {

    contains(QMAKE_CC, gcc){
    message(Qt5.15.14)
        # gcc
        # CONFIG (release, debug|release): LIBS += -L../lib5/ -labout # Release
        # else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -laboutd # Debug
         LIBS += -L../lib5/ -labout
         LIBS += -L../lib5/ -lcheckupdate
         unix:LIBS += -L../lib5/ -lupdateappimage
         LIBS += -L../lib5/ -lcreateshortcut
         win32:LIBS += -L../lib5/ -ldownload_install
    }
      # Visual C++
    contains(QMAKE_CC, cl){
        # CONFIG (release, debug|release): LIBS += -L../lib5_msvc/ -labout # Release
        # else: CONFIG (debug, debug|release): LIBS += -L../../lib5_msvc/ -laboutd # Debug
         LIBS += -L../lib5_msvc/ -labout
         LIBS += -L../lib5_msvc/ -lcheckupdate
         LIBS += -L../lib5_msvc/ -lcreateshortcut
    }
}


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
