// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

// qt-style Copyright (C) 2024 Ingemar Ceicer
// https://gitlab.com/posktomten/qt-style/wikis/home
// programming@ceicer.com

// A program to test how a Qt GUI program looks depending on which "Style" you choose.
// Which styles are available to choose from depends on the operating system and Qt version.

// qt-style is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
// And NO LATER versions are allowed.
// GPL Version 3.0 only.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "mainwindow.h"
#include "about.h"
#include <QDate>
void MainWindow::about()
{
    const QString *purpose = new QString(QStringLiteral(""));
    QString *copyright_year = new QString;

    if(COPYRIGHT_YEAR == QString::number(QDate::currentDate().year())) {
        *copyright_year = QString::number(QDate::currentDate().year());
    } else {
        *copyright_year = COPYRIGHT_YEAR " - " + QString::number(QDate::currentDate().year());
    }

    // QString::number(QDate::currentDate().year());
    const QString *translator = new QString(QStringLiteral(""));
    const QPixmap *pixmap = new QPixmap(QStringLiteral(":/images/icon.png"));
    // const QPixmap *pixmap = nullptr;
    About *about = new About(QStringLiteral(DISPLAY_NAME),
                             QStringLiteral(VERSION),
                             QStringLiteral(COPYRIGHT),
                             QStringLiteral(EMAIL),
                             copyright_year,
                             QStringLiteral(BUILD_DATE_TIME),
                             QStringLiteral(LICENSE),
                             QStringLiteral(LICENSE_LINK),
                             QStringLiteral(CHANGELOG),
                             QStringLiteral(SOURCECODE),
                             QStringLiteral(WEBSITE),
                             QStringLiteral(COMPILEDON),
                             purpose,
                             translator,
                             pixmap,
                             false
                            );
    delete about;
}
