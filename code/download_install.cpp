//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Program name
//          Copyright (C) 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include <qglobal.h>


#include "mainwindow.h"


#include <QMessageBox>
#include <QAbstractButton>
#include <QCoreApplication>
#include <QFileInfo>
#include <QProcess>
#include <QDir>
#include "mainwindow.h"
#include "qpushbutton.h"


#if defined Q_OS_LINUX
#include "createshortcut_lin.h"
#elif defined Q_OS_WIN
#include "download_install.h"
#include "createshortcut_win.h"
#endif


#if (defined DOWNLOAD_INSTALL) && (defined Q_OS_WIN)
void MainWindow::downloadInstall()
{
    QMessageBox *msgBox = new QMessageBox(this);
    // msgBox->setInformativeText("<a style=\"text-decoration:none\" href = \""  WEBSITE   "\">" + tr("To the website") + "</a>");
    msgBox->setIcon(QMessageBox::Information);
    msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
    msgBox->setText(tr("Want to download and install the latest version of ") + DISPLAY_NAME + " ? ");
    QPushButton *pbYes = msgBox->addButton(tr("Yes"), QMessageBox::AcceptRole);
    QPushButton *pbCancel = msgBox->addButton(tr("Cancel"), QMessageBox::RejectRole);
    pbYes->setFocus();
    pbYes->setDefault(true);
    msgBox->exec();

    if(msgBox->clickedButton() == pbCancel) {
        return;
    } else if(msgBox->clickedButton() == pbYes) {
        close();
        DownloadInstall *mDownloadInstall(nullptr);
        mDownloadInstall = new DownloadInstall;
        QString *path = new QString(QStringLiteral(PATH));
        QString *filename = new QString(QStringLiteral(FILENAME));
        QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
        QString *version = new QString(QStringLiteral(VERSION));
        mDownloadInstall->setValues(path, filename, display_name, version);
        mDownloadInstall->show();
    }
}

void MainWindow::uninstall()
{
    QMessageBox *msgBox = new QMessageBox(this);
    // msgBox->setInformativeText("<a style=\"text-decoration:none\" href = \""  WEBSITE   "\">" + tr("To the website") + "</a>");
    msgBox->setIcon(QMessageBox::Information);
    msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
    msgBox->setText(tr("Do you want to uninstall ") + DISPLAY_NAME + "?");
    QPushButton *pbNo = msgBox->addButton(tr("No"), QMessageBox::RejectRole);
    QPushButton *pbYes = msgBox->addButton(tr("Yes"), QMessageBox::AcceptRole);
    msgBox->setDefaultButton(pbNo);
    msgBox->exec();

    if(msgBox->clickedButton() == pbNo) {
        return;
    }

    if(msgBox->clickedButton() == pbYes) {
        const QString EXECUTE =
            QCoreApplication::applicationDirPath() + "/" +
            "uninstall.exe";
        //
        QFileInfo fi(EXECUTE);

        if(!fi.isExecutable()) {
            QMessageBox *msgBox = new QMessageBox(this);
            msgBox->setIcon(QMessageBox::Critical);
            msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox->setText(tr("An unexpected error occurred.<br>") + "\"" + QDir::toNativeSeparators(EXECUTE) + "\"" + tr("<br>can not be found or is not an executable program."));
            msgBox->exec();
            return;
        }

        QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
        Createshortcut::removeDesktopShortcutSilent(executable_name);
        QProcess p;
        p.setProgram(EXECUTE);
        p.startDetached();
        savesettings = false;
        close();
        return;
    }
}

#endif // defined DOWNLOAD_INSTALL && defined Q_OS_WIN
