// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

// qt-style Copyright (C) 2024 Ingemar Ceicer
// https://gitlab.com/posktomten/qt-style/wikis/home
// programming@ceicer.com

// A program to test how a Qt GUI program looks depending on which "Style" you choose.
// Which styles are available to choose from depends on the operating system and Qt version.

// qt-style is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
// And NO LATER versions are allowed.
// GPL Version 3.0 only.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "mainwindow.h"
#include <QStyleFactory>
#include <QApplication>
#include <QSettings>
#include <QGuiApplication>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    // qputenv("QT_QPA_PLATFORM", "windows:darkmode=1");
    // Qt::ColorScheme(Dark);
    // Qt::ColorScheme::Dark
    // QGuiApplication::setStyle();
    MainWindow *w = new MainWindow;
    w->show();
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Style");
    QString currentstyle = settings.value(QStringLiteral("currentstyle"), QStringLiteral("Fusion")).toString();
    settings.endGroup();
    QApplication::setStyle(QStyleFactory::create(currentstyle));
    QObject::connect(&a, &QCoreApplication::aboutToQuit,
                     [w]() -> void { w->setEndConfig(); });
    return a.exec();
}
