//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          CREATESHORTCUT (Windows)
//          Copyright (C) 2022 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/libcreateshortcut
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef CREATECHORTCUT_WIN_H
#define CREATECHORTCUT_WIN_H

#include <QWidget>

// #if defined(Q_OS_LINUX)
// #if defined(CREATECHORTCUT_LIBRARY)
// #include "createshortcut_global.h"
// class CREATECHORTCUT_EXPORT Createshortcut : public QWidget
// #else
// class Createshortcut : public QWidget
// #endif // CREATECHORTCUT_LIBRARY
// #else
// class Createshortcut : public QWidget
// #endif // Q_OS_LINUX
// {
class Createshortcut : public QWidget
{
    Q_OBJECT

public:

    static void makeShortcutFile(QString *display_name, QString *executable_name, bool applications, bool desktop);
    static void removeApplicationShortcut(QString *executable_name);
    static void removeDesktopShortcut(QString *executable_name);
    static void removeDesktopShortcutSilent(QString *executable_name);
    static bool deleteAllSettings(QString *display_name, QString *execcutable_name, QString *version, bool download_install);
private:
    static void removeApplicationShortcutSilent(QString *display_name);


//#endif
};

#endif // CREATECHORTCUT_WIN_H
