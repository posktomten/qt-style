# Qt style

<h3>Try different Qt styles</h3>

Windows and Linux<br>
Qt5 and Qt6

[Images](https://gitlab.com/posktomten/qt-style/-/wikis/home) | [Downloads](https://bin.ceicer.com/qt-style/bin/) | [LICENSE](https://gitlab.com/posktomten/qt-style/-/blob/master/LICENSE%20for%20qt-style?ref_type=heads)




```
CHANGELOG

2024-06-18
Version 1.0.3
Linux: The AppImage updates itself.

2024-05-05 
Version 1.0.2
The application can check for updates.

2024-05-05
Version 1.0.1
"About" dialog.
Delete all settings and exit.

2024-05-04
Version 1.0.0
First release.
```

<img src="https://bin.ceicer.com/qt-style/Qt5.7.1_liten_Ubuntu.png" alt="Qt-style">
